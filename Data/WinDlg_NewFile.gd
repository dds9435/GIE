extends WindowDialog


func _ready():
	pass


func _on_Btn_Create_pressed():
	var main_node = get_tree().get_root().get_node("Control")
	main_node.image = main_node.new_image(
			$"VBoxContainer/HBoxContainer/Image_X".value, 
			$"VBoxContainer/HBoxContainer/Image_Y".value)
	main_node.set_data_canvas_from_img()
	self.hide()