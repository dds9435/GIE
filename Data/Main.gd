extends Control
# License: MIT
# (C) Dmitry Derbin & Georgy Linkovsky 


onready var canvas = $"UI/Canvas"
onready var background = $"UI/Canvas/BackgroundImage"

var image
var zoom_step = 1
var zoom_default = Vector2(1, 1)
var cur_pos = Vector2()
var file_name = "" 

onready var StatusPanel = $"UI/Info/HBoxContainer"
onready var FileDlg = $"UI/FileDlg"
onready var FM_panel = $"UI/FileManagmentPanel"
onready var NewFileDlg = $"UI/NewFileDlg"


func _ready():
	OS.set_window_maximized(true)


# Слот для сигнала от кнопки "Новый файл"
func _on_Btn_New_pressed():
	NewFileDlg.show()


# Слот для сигнала от кнопки "Открыть файл"
func _on_Btn_Open_pressed():
	FileDlg.mode = FileDialog.MODE_OPEN_FILE
	FileDlg.access = FileDialog.ACCESS_FILESYSTEM
	FileDlg.show()


# Слот для сигнала от кнопки "Сохранить"
func _on_Btn_Save_pressed():
	if file_name:
		pass
	else:
		_on_Btn_SaveAs_pressed()


# Слот для сигнала от кнопки "Сохранить как"
func _on_Btn_SaveAs_pressed():
	FileDlg.mode = FileDialog.MODE_SAVE_FILE
	FileDlg.access = FileDialog.ACCESS_FILESYSTEM
	FileDlg.show()


# Слот для сигнала от кнопки "Настройки"
func _on_Btn_Settings_pressed():
	pass


# Слот для сигнала от выбора файла
func _on_FileDlg_file_selected(path):
	if FileDlg.mode == FileDialog.MODE_SAVE_FILE:
		if image != null:
			image.save_png(path)
		
	elif FileDlg.mode == FileDialog.MODE_OPEN_FILE:
		var image_object = ImageTexture.new()
		image_object.load(path)
		image_object.flags = Texture.FLAG_VIDEO_SURFACE
		image = image_object.get_data()
		canvas.texture = image_object
		canvas.rect_size = image_object.get_size()
		canvas.visible = true


# Ф-ция для отрисовки Image на холст
func set_data_canvas_from_img(img=image):
	var image_object = ImageTexture.new()
	image_object.create_from_image(img)
	image_object.flags = Texture.FLAG_VIDEO_SURFACE
	canvas.texture = image_object
	canvas.rect_size = image_object.get_size()
	canvas.visible = true
	#ImgSize".text = "X:" + str(image_x.value) + " Y:" + str(image_y.value)


# Ф-ция для создания изображения
func new_image(x=1, y=1, format = Image.FORMAT_RGBA8):
	var image = Image.new()
	image.create(x, y, false, format)
	image.lock()
	return image


func _process(delta):
	if canvas.texture:
		# Зум
		if Input.is_action_just_pressed("zoom+"):
			canvas.rect_size += canvas.texture.get_size() * zoom_step
		elif Input.is_action_just_pressed("zoom-"):
			canvas.rect_size -= canvas.texture.get_size() * zoom_step
		
		# Переход к изображению
		elif Input.is_action_just_pressed("to_img"):	
			canvas.set_margins_preset(Control.PRESET_CENTER)
		
		# Обнуляем зум
		elif Input.is_action_just_pressed("zoom_default"):	
			canvas.rect_size = canvas.texture.get_size()
	
		
		# Передвижение холста
		if Input.is_action_just_pressed("move"):
			cur_pos = canvas.rect_position - get_global_mouse_position()
		if Input.is_action_pressed("move"):
			if get_global_mouse_position() + cur_pos >= - OS.window_size/2:
				canvas.rect_position = get_global_mouse_position() + cur_pos

